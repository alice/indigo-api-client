const priorRoute = 'prior'
const posteriorRotue = 'posterior'


class Indigo {

    constructor(endpoint) {
        this.endpoint = endpoint
    }


    async request(method, route, body, headers={}) {

        const url = this.getURL(route)

        const response = await fetch(url, {
            mode: 'cors',
            method,
            headers,
            body,
        })

        return response.json()
    }


    get(...rest) {
        return this.request('GET', ...rest)
    }


    post(...rest) {
        return this.request('POST', ...rest)
    }


    getURL(route) {
        return this.endpoint + '/' + route
    }


    getPrior() {
        return this.get(priorRoute)
    }


    postPosterior(priorDistribution, symptomsMap) {

        const data = new FormData()

        data.append('priorDistribution', priorDistribution)
        data.append('symptomsMap', symptomsMap)

        return this.post(posteriorRoute, data)
    }
}


export default Indigio
